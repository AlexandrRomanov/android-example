package com.example.example;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.example.model.NewsData;

import java.util.List;

import static android.content.ContentValues.TAG;

public class newsListAdapter extends RecyclerView.Adapter<newsListAdapter.newsViewHolder> {

    private static final int MAX_IMG_WIDTH = 98;

    public List<NewsData> news;

    private String rubric;

    public void setRubric(String rubric) {
        this.rubric = rubric;
    }

    public newsListAdapter(List<NewsData> news, String rubric) {
        this.news = news;
        this.rubric = rubric;
    }

    @Override
    public newsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_card, parent, false);

        newsViewHolder vh = new newsViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(newsViewHolder newsViewHolder, int position) {
        String title = news.get(position).getTitle();

        newsViewHolder.nameView.setText(title);
    }

    @Override
    public int getItemCount() {
        return this.news.size();
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    class newsViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView nameView;

        public CardView placeCardView;

        public newsViewHolder(View v) {
            super(v);

            this.placeCardView = v.findViewById(R.id.news_card);
            this.nameView = v.findViewById(R.id.list_text);

            final Context currentContext = v.getContext();

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int positionOfItemClicked = getLayoutPosition();
                    if (RecyclerView.NO_POSITION != positionOfItemClicked) {

                        String placeClicked = news.get(positionOfItemClicked).getTitle();
                        System.out.println(">>> click! " + placeClicked);

                        Intent viewNewsIntent = new Intent(currentContext, ScrollingActivity.class);
                        viewNewsIntent.putExtra("description", news.get(positionOfItemClicked).getDescription());
                        viewNewsIntent.putExtra("newsUrl", news.get(positionOfItemClicked).getUrl());
                        viewNewsIntent.putExtra("rubric", rubric);

                        currentContext.startActivity(viewNewsIntent);

                    } else {
                        Log.w(TAG, "position of item clicked = -1");
                    }
                }
            });
        }
    }
}
