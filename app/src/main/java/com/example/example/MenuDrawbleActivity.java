package com.example.example;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;

import com.example.example.model.InternetRequest;
import com.example.example.model.NewsData;
import com.example.example.model.SettingsActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.example.example.model.SettingsActivity.DEFAULT_LIST_SIZE;
import static com.example.example.model.SettingsActivity.SETTINGS_KEY_LIST_SIZE;


import static android.content.ContentValues.TAG;
public class MenuDrawbleActivity extends AppCompatActivity
        implements SharedPreferences.OnSharedPreferenceChangeListener, NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView recyclerView;
    private newsListAdapter itemListAdapter;
    private RecyclerView.LayoutManager itemListLayoutManager;
    List<NewsData> listToShow = new ArrayList<>();
    ArrayList<NewsData> newsData = new ArrayList<>();
    String userName;
    TableLayout usersMenu;
    String rubric = "sports";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userName = "admin";
        setContentView(R.layout.activity_menu_drawble);
        if (getIntent().getExtras() != null) {
            rubric = getIntent().getExtras().getString("rubric");
        }
        itemListAdapter = new newsListAdapter(listToShow, rubric);
        sendInternetRequest();
        recyclerView = (RecyclerView) findViewById(R.id.list_of_news);
        itemListLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(itemListLayoutManager);
        recyclerView.setAdapter(itemListAdapter);
        usersMenu = (TableLayout) findViewById(R.id.users_menu);
        ((Button) findViewById(R.id.business)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                rubric = "business";
                itemListAdapter.setRubric(rubric);
                usersMenu.setVisibility(View.INVISIBLE);
            }
        });
        ((Button) findViewById(R.id.health)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                rubric = "health";
                itemListAdapter.setRubric(rubric);
                usersMenu.setVisibility(View.INVISIBLE);
            }
        });
        ((Button) findViewById(R.id.sport)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rubric = "sports";
                itemListAdapter.setRubric(rubric);
                usersMenu.setVisibility(View.INVISIBLE);
            }
        });
        ((Button) findViewById(R.id.general)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rubric = "general";
                itemListAdapter.setRubric(rubric);
                usersMenu.setVisibility(View.INVISIBLE);
            }
        });
        ((Button) findViewById(R.id.science)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rubric = "science";
                itemListAdapter.setRubric(rubric);
                usersMenu.setVisibility(View.INVISIBLE);
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        ((TextView) header.findViewById(R.id.textView3)).setText(userName);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_drawble, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            sendInternetRequest();
            updateListSize();
        }
        if (id == R.id.action_rubrics) {
            if (usersMenu.getVisibility() == View.VISIBLE) {
                usersMenu.setVisibility(View.INVISIBLE);
            } else {
                usersMenu.setVisibility(View.VISIBLE);
            }
            return true;
        }
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        updateListSize();
    }

    public void updateListSize() {
        System.out.println("updating properties");
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        int itemsToShow = Integer.parseInt(pref.getString(SETTINGS_KEY_LIST_SIZE, DEFAULT_LIST_SIZE));

        listToShow.clear();
        listToShow.addAll(newsData.size() > itemsToShow ? newsData.subList(0, itemsToShow) : newsData);

        itemListAdapter.notifyDataSetChanged();
    }

    @SuppressLint("StaticFieldLeak")
    private void sendInternetRequest() {
        String API_KEY = "339a2a579dab4bd0a73ba8c1d0a2012f";

        Uri uri = Uri.parse("https://newsapi.org/v2/top-headlines").buildUpon()
                .appendQueryParameter("country", "ru")
                .appendQueryParameter("category", rubric)
                .appendQueryParameter("apiKey", API_KEY)
                .build();

        URL url = null;
        try {
            url = new URL(uri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        InternetRequest internetRequest = new InternetRequest();

        String requestResult = null;
        try {
            assert url != null;
            requestResult = internetRequest.execute(url.toString()).get();
            Type type = new TypeToken<ArrayList<NewsData>>(){}.getType();
            newsData = new Gson().fromJson(requestResult, type);
            updateListSize();
            Log.i(TAG, requestResult);

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
}
