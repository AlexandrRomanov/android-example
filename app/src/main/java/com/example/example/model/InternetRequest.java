package com.example.example.model;

import android.os.AsyncTask;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class InternetRequest extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... strings) {
        ArrayList<NewsData> newsArray = new ArrayList<>();

        try {
            URL url = new URL(strings[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            InputStream stream = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder builder = new StringBuilder();

            String inputString;
            while ((inputString = bufferedReader.readLine()) != null) {
                builder.append(inputString);
            }

            JSONObject topLevel = new JSONObject(builder.toString());
            JSONArray articles = topLevel.getJSONArray("articles");

            for (int i = 0; i < articles.length(); i++) {
                JSONObject news = articles.getJSONObject(i);

                newsArray.add(new NewsData(
                        news.getJSONObject("source").getString("name"),
                        news.getString("title"),
                        news.getString("description"),
                        news.getString("url"),
                        news.getString("urlToImage"),
                        news.getString("publishedAt")
                ));
            }

            urlConnection.disconnect();
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return new Gson().toJson(newsArray);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}
