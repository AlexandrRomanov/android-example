package com.example.example.model;

import android.os.Parcel;
import android.os.Parcelable;

public class NewsData implements Parcelable {
    private final String sourceName;
    private final String title;
    private final String description;
    private final String url;
    private final String urlToImage;
    private final String publishedAt;

    public NewsData(String sourceName, String title, String description, String url, String urlToImage, String publishedAt) {
        this.sourceName = sourceName;
        this.title = title.equals("null") ? "" : title;
        this.description = description.equals("null") ? "" : description;
        this.url = url;
        this.urlToImage = urlToImage.equals("null") ? "" : urlToImage;
        this.publishedAt = publishedAt;
    }

    private NewsData(Parcel parcel) {
        this.sourceName = parcel.readString();
        this.title = parcel.readString();
        this.description = parcel.readString();
        this.url = parcel.readString();
        this.urlToImage = parcel.readString();
        this.publishedAt = parcel.readString();
    }
    public String getSourceName() {
        return sourceName;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public String getUrlToImage() {
        return urlToImage;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(sourceName);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(url);
        parcel.writeString(urlToImage);
        parcel.writeString(publishedAt);
    }
    public static final Parcelable.Creator<NewsData> CREATOR = new Parcelable.Creator<NewsData>() {
        public NewsData createFromParcel(Parcel in) {
            return new NewsData(in);
        }

        public NewsData[] newArray(int size) {
            return new NewsData[size];
        }
    };
}
