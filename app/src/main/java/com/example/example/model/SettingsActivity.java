package com.example.example.model;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.example.example.R;

public class SettingsActivity extends PreferenceActivity {

    public static final String SETTINGS_KEY_LIST_SIZE = "list_size";

    public static final String DEFAULT_LIST_SIZE = "5";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref);
    }

}