package com.example.example;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class ScrollingActivity extends AppCompatActivity {

    private String description;
    private String newsUrl;
    private String rubric;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        description = getIntent().getExtras().getString("description");
        newsUrl = getIntent().getExtras().getString("newsUrl");
        rubric = getIntent().getExtras().getString("rubric");
        this.setTitle(rubric + " news");
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) findViewById(R.id.description)).setText(description + "\n" + newsUrl);
        // userInfo.toString()
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Спасибо", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ScrollingActivity.this,
                        MenuDrawbleActivity.class);
                intent.putExtra("rubric", rubric);
                startActivity(intent);
                finish();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ScrollingActivity.this,
                MenuDrawbleActivity.class);
        intent.putExtra("rubric", rubric);
        startActivity(intent);
        finish();
    }
}
