package com.example.example;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.example.example.model.InternetRequest;
import com.example.example.model.NewsData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static android.content.ContentValues.TAG;

public class NewsLoader {

    private final static String API_KEY = "339a2a579dab4bd0a73ba8c1d0a2012f";
    public void getNews() {
        Uri uri = Uri.parse("https://newsapi.org/v2/top-headlines").buildUpon()
                .appendQueryParameter("country", "ru")
                .appendQueryParameter("category", "sports")
                .appendQueryParameter("apiKey", API_KEY)
                .build();
        URL url = null;
        try {
            url = new URL(uri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        InternetRequest internetRequest = new InternetRequest();

        String requestResult;
        try {
            assert url != null;
            requestResult = internetRequest.execute(url.toString()).get();
            Type type = new TypeToken<ArrayList<NewsData>>(){}.getType();
            ArrayList<NewsData> newsData = new Gson().fromJson(requestResult, type);
            Log.i(TAG, requestResult);

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}
